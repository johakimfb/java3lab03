package dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void test_echo_returns5()
    {
        App app = new App();
        assertEquals("Make sure that the echo() method returns what we give it", 5, app.echo(5), 0);
    }
    @Test
    public void test_oneMore_return3()
    {
        App app = new App();
        assertEquals("Make sure that when given an int, will return int+1", 3, app.oneMore(2), 0);

    }
}
